<?php

/**
* Подключаем двигало
*/
require dirname(__FILE__) . "/system.php";

$engine = new LightEngine();



try
{
	echo "root: " . DIR_ROOT . "\n";
	echo $engine->system->read('username.minlen', 3) . "\n";
	echo $engine->system->read('username.maxlen', 10) . "\n";
	$engine->system->write('test.foo', 50);
	$engine->system->commit();
}
catch (db_exception $e)
{
	echo "DB error: " . $e->getDriverMessage() . "\n";
	echo "Query: " . $e->getQuery() . "\n";
}



?>