<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
	<title>{IF defined(TITLE) TITLE ELSE IF defined(project) project.title ELSE "Murad Baste" ENDIF ENDIF}</title>
	{FOREACH HTTP as meta}
	<meta http-equiv="{escape(meta.name)}" content="{escape(meta.value)}" />
	{ENDEACH}
	{FOREACH META as meta}
	<meta name="{escape(meta.name)}" content="{escape(meta.value)}" />
	{ENDEACH}
	{FOREACH LINKS as link}
	<link rel="{escape(link.rel)}" href="{escape(link.URL)}" />
	{ENDEACH}
	<script type="text/javascript">
		var ajax_prefix = '{INFO.PREFIX}';
	</script>
	{FOREACH SCRIPTS as script}
	<script type="{escape(script.type)}" src="{escape(script.URL)}"></script>
	{ENDEACH}
	{INCLUDE "std/headers"}
</head>
{INCLUDE LAYOUT}
</html>
