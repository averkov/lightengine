<?php

/**
* Каталог с тестами
*/
define('DIR_TESTS', dirname(__FILE__));

/**
* Корень движка/сайта
*/
define('DIR_ROOT', dirname(DIR_TESTS));

/**
* Пути поиска классов
*/
define('DIR_PATH', 'modules');

/**
* Двигатель
*/
require_once DIR_ROOT . "/engine.php";

/**
* Каталог со скинами
*/
define('DIR_THEMES', makepath(DIR_TESTS, 'themes'));

/**
* Каталог для кеша
*/
define('DIR_CACHE', makepath(DIR_TESTS, 'cache'));

/**
* Язык по умолчанию
*/
define('LANG_DEFAULT', 'russian');

/**
* Каталог с файлами локализации
*/
define('DIR_LANGS', makepath(DIR_TESTS, 'lang'));

/**
* Каталог для описаний форм
*/
define('DIR_FORMS', makepath(DIR_TESTS, 'forms'));

/**
* Загрузчик классов
*/
function __autoload($class)
{
	LightEngine::loadClass($class);
}

$engine = new LightEngine();

?>