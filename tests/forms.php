<?php

/**
* Подключаем двигало
*/
require dirname(__FILE__) . "/system.php";

$engine = new LightEngine();




try
{
	$engine->session->init();
	
	if ( ! isset($_POST['action']) )
	{
		$form = $engine->forms->newForm('document.create');
		echo $form->render();
	}
	else
	{
		$form = $engine->forms->parse('document.create');
		if ( $form->hasErrors() )
		{
			echo $form->render();
		}
		else
		{
			header('content-type: text/plain; charset=utf-8');
			print_r($form->values);
		}
	}
}
catch (Exception $e)
{
	$class = get_class($e);
	$message = $e->getMessage();
	echo "Error ($class): $message\n";
}



?>