<?php

/**
* Подключаем двигало
*/
require dirname(__FILE__) . "/system.php";

$engine = new LightEngine();


try
{
	$r = $engine->db->select('config', '*', '', 'config_name ASC');
	while( $f = $engine->db->fetchAssoc($r) )
	{
		echo "$f[config_name]: $f[config_value]\n";
	}
	$engine->db->freeResult($r);
	echo "\n";
}
catch(db_exception $e)
{
	echo "Database error: " . $e->getMessage() . "\n";
	echo "Query:\n";
	echo $e->getQuery();
	echo "\n\n";
}

?>