<?php

/**
* Подключаем двигало
*/
require dirname(__FILE__) . "/system.php";

$engine = new LightEngine();

/**
* Извлечь случайную марковскую цепочку
* @param SQLEngine драйвер БД
* @param int длина цепочки
* @retval mixed цепочка в виде списка строк или FALSE
*/
function getMarkovChain(SQLEngine $db, $len)
{
	// неверные входные данные
	if ( $len <= 0 ) return false;
	
	// составим списки выбираемых полей, таблиц и ограничей
	$table = 'pairs';
	
	// первая фраза
	$fields = array ("p0.pair_left");
	$tables = array ("$table AS p0");
	$where = array ("p0.pair_first = 1");
	
	if ( $len > 1 )
	{
		// внутрение фразы
		$len--;
		for($i = 1, $j = 0; $i < $len; $i++, $j++)
		{
			$fields[] = "p$i.pair_left";
			$tables[] = "$table AS p$i";
			$where[] = "p$i.pair_left = p$j.pair_right AND p$i.pair_inside = 1";
		}
		
		// последняя фраза
		$fields[] = "p$i.pair_left";
		$tables[] = "$table AS p$i";
		$where[] = "p$i.pair_left = p$j.pair_right AND p$i.pair_last = 1";
	} else {
		// цепочка длины 1, фраза должна быть начальной и конечной
		$where[] = "p0.pair_last = 1";
	}
	
	$fields = implode(", ", $fields);
	$tables = implode(", ", $tables);
	$where = implode(" AND ", $where);
	
	// читаем число всех цепочек
	// Примечание, чтобы уполучить число уникальных цепочек нужно
	// уазать COUNT(DISTINCT $fields)
	$r = $db->query($sql = "SELECT COUNT(*) FROM $tables WHERE $where");
	$row = $db->fetchRow($r);
	$db->freeResult($r);
	$count = $row[0];
	if ( $count < 1 ) return false;
	
	// выбираем случайное смещение и цепочку с выбранным смещением
	// Примечание: для выбора среди уникальных цепочек нужно указать
	// SELECT DISTINCT $fields...
	$offset = mt_rand(0, $count-1);
	$r = $db->query("SELECT $fields FROM $tables WHERE $where LIMIT 1 OFFSET $offset");
	$row = $db->fetchRow($r);
	$db->freeResult($r);
	
	// возращаем цепочку в виде списка
	return $row;
}

try
{
	for($i = 1; $i <= 5; $i++)
	{
		$chain = getMarkovChain($engine->db, 4);
		$message = $chain ? implode(' ', $chain) : "no message :-(";
		echo "$i: $message\n";
	}
}
catch (Exception $e)
{
	echo "Error (" . get_class($e) . "): " . $e->getMessage() . "\n";
}



?>