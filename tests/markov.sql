SET NAMES UTF8;

DROP TABLE IF EXISTS pairs;
CREATE TABLE pairs
(
	pair_left VARCHAR(80) NOT NULL,
	pair_right VARCHAR(80) NOT NULL,
	
	pair_first TINYINT(1) NOT NULL DEFAULT 0,
	pair_inside TINYINT(1) NOT NULL DEFAULT 0,
	pair_last TINYINT(1) NOT NULL DEFAULT 0,
	
	UNIQUE INDEX pair (pair_left, pair_right),
	
	INDEX first_pair (pair_first, pair_left),
	INDEX inside_pair (pair_inside, pair_left),
	INDEX last_pair (pair_last, pair_left)
);

INSERT INTO pairs (pair_left, pair_right, pair_first, pair_inside, pair_last) VALUES
	('Hello', 'Alice', 1, 0, 1),
	('Hello', 'Bob', 1, 0, 1),
	('Hello', 'John', 1, 0, 1),
	
	('Alice', 'I have', 0, 1, 1),
	('Bob', 'I have', 0, 1, 1),
	('John', 'I have', 0, 1, 1),
	
	('Alice', 'I want', 0, 1, 1),
	('Bob', 'I want', 0, 1, 1),
	('John', 'I want', 0, 1, 1),
	
	('I have', 'coffee', 1, 1, 0),
	('I have', 'tia', 1, 1, 0),
	('I have', 'banana', 1, 1, 0),
	
	('I want', 'coffee', 1, 1, 0),
	('I want', 'tia', 1, 1, 0),
	('I want', 'banana', 1, 1, 0),
	
	('coffee', '', 0, 0, 1),
	('tia', '', 0, 0, 1),
	('banana', '', 0, 0, 1);
