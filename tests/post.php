<?php

/**
* Подключаем двигало
*/
require dirname(__FILE__) . "/system.php";

header('Content-type: text/plain; charset=UTF-8');

$engine = new LightEngine();

try
{
	$form = $engine->post->parse('user/register');
	$tag = $form->makeFormTag();
	print_r($tag);
}
catch (Exception $e)
{
	$class = get_class($e);
	$message = $e->getMessage();
	echo "Error ($class): $message\n";
}



?>